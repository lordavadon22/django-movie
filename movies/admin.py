from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django import forms
from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import *


class ReviewInline(admin.TabularInline):
    model = Reviews
    extra = 1
    readonly_fields = ('name', 'email',)


class MovieShotsInline(admin.TabularInline):
    model = MovieShots
    extra = 1
    readonly_fields = ('show_image',)

    def show_image(self, obj):
        if obj.image:
            return mark_safe(f'<img src="{obj.image.url}" width="100">')
        return '-'

    show_image.short_description = 'Миниатюра'


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'url',)


class MovieAdminForm(forms.ModelForm):
    description = forms.CharField(label='Описание', widget=CKEditorUploadingWidget())

    class Meta:
        model = Movie
        fields = '__all__'


class MovieAdmin(admin.ModelAdmin):
    list_display = ('title', 'category', 'url', 'draft')
    list_editable = ('draft',)
    list_filter = ('category', 'year',)
    search_fields = ('title', 'category__name',)
    readonly_fields = ('show_image',)
    inlines = (MovieShotsInline, ReviewInline,)
    form = MovieAdminForm
    actions = ('publish', 'unpublish',)
    save_on_top = True
    save_as = True
    fieldsets = (
        (None, {
            'fields': (('title', 'tagline',),)
        }),
        (None, {
            'fields': ('description', ('poster', 'show_image',),)
        }),
        (None, {
            'fields': (('year', 'world_premiere', 'country',),)
        }),
        ('Режисеры, Актеры, Жанры, Категории', {
            'classes': ('collapse',),
            'fields': (('directors', 'actors', 'genres', 'category',),)
        }),
        (None, {
            'fields': ('budget', 'fees_in_usa', 'fees_in_world',)
        }),
        ('Опции', {
            'fields': (('url', 'draft',),)
        }),
    )

    def unpublish(self, request, queryset):
        """
        Снять с публикации
        """
        row_update = queryset.update(draft=True)
        if row_update == 1:
            message_bit = '1 запись обновлена'
        else:
            message_bit = f'{row_update} записей обновлены'
        self.message_user(request, message_bit)

    unpublish.short_description = 'Снять с публикации'
    unpublish.allow_permissions = ('change',)

    def publish(self, request, queryset):
        """
        Опубликовать
        """
        row_update = queryset.update(draft=False)
        if row_update == 1:
            message_bit = '1 запись обновлена'
        else:
            message_bit = f'{row_update} записей обновлены'
        self.message_user(request, message_bit)

    publish.short_description = 'Опубликовать'
    publish.allow_permissions = ('change',)

    def show_image(self, obj):
        if obj.image:
            return mark_safe(f'<img src="{obj.poster.url}" width="50">')
        return '-'

    show_image.short_description = 'Постер'


class ReviewAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'parent', 'movie')
    readonly_fields = ('name', 'email',)


class GenreAdmin(admin.ModelAdmin):
    list_display = ('name', 'url',)


class ActorAdminForm(forms.ModelForm):
    description = forms.CharField(label='Описание', widget=CKEditorUploadingWidget())

    class Meta:
        model = Actor
        fields = '__all__'


class ActorAdmin(admin.ModelAdmin):
    list_display = ('name', 'age', 'show_image',)
    readonly_fields = ('show_image',)
    form = ActorAdminForm

    def show_image(self, obj):
        if obj.image:
            return mark_safe(f'<img src="{obj.image.url}" width="50">')
        return '-'

    show_image.short_description = 'Миниатюра'


class RatingAdmin(admin.ModelAdmin):
    list_display = ('star', 'movie', 'ip',)


class MovieShotsAdmin(admin.ModelAdmin):
    list_display = ('title', 'movie', 'show_image',)
    readonly_fields = ('show_image',)

    def show_image(self, obj):
        if obj.image:
            return mark_safe(f'<img src="{obj.image.url}" width="50">')
        return '-'

    show_image.short_description = 'Миниатюра'


admin.site.register(Category, CategoryAdmin)
admin.site.register(Actor, ActorAdmin)
admin.site.register(Genre, GenreAdmin)
admin.site.register(Movie, MovieAdmin)
admin.site.register(MovieShots, MovieShotsAdmin)
admin.site.register(RatingStar)
admin.site.register(Rating, RatingAdmin)
admin.site.register(Reviews, ReviewAdmin)
