from django.urls import path, include

from movies import views

urlpatterns = [
    path('', views.MovieListView.as_view(), name='movies'),
    path('filter/', views.FilterMoviesView.as_view(), name='filter'),
    path('search/', views.Search.as_view(), name='search'),
    path('json-filter/', views.JsonFilterMoviesView.as_view(), name='json_filter'),
    path('add-rating/', views.AddStarRating.as_view(), name='add_rating'),
    path('movie/<slug:slug>/', views.MovieDetailView.as_view(), name='movie_detail'),
    path('review/<int:pk>/add/', views.AddReviews.as_view(), name='add_review'),
    path('actor/<str:slug>/', views.ActorView.as_view(), name='actor_detail'),
]
