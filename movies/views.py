from django.db.models import Q, Avg
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import DetailView, ListView, CreateView

from .forms import ReviewForm, RatingForm
from .models import Movie, Reviews, Actor, Genre, Rating


class GenreYear:
    """
    Жанры и года выхода фильмов
    """

    def get_genres(self):
        return Genre.objects.all()

    def get_years(self):
        return Movie.objects.filter(draft=False).values('year')


class MovieListView(GenreYear, ListView):
    """
    Список фильмов
    """
    queryset = Movie.objects.filter(draft=False)
    template_name = 'movies/movie_list.html'
    context_object_name = 'movie_list'
    paginate_by = 5


class MovieDetailView(GenreYear, DetailView):
    """
    Детали о фильме
    """
    model = Movie
    template_name = 'movies/movie_detail.html'
    context_object_name = 'movie'
    slug_field = 'url'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        stars = self.get_user_stars(**self.kwargs)
        if stars:
            context['stars'] = str(stars)
        rating = Rating.objects.filter(movie_id=self.object.id).aggregate(average_rating=Avg('star__value'))
        if rating:
            context['average_rating'] = rating['average_rating']
        context['star_form'] = RatingForm()
        context['form'] = ReviewForm()
        return context

    def get_user_stars(self, **kwargs):
        ip = AddStarRating.get_client_ip(self, self.request)
        movie_id = Movie.objects.get(url=kwargs['slug']).id
        if Rating.objects.filter(ip=ip, movie_id=movie_id).exists():
            return Rating.objects.get(ip=ip, movie_id=movie_id).star
        else:
            return None


class AddReviews(CreateView):
    mode = Reviews
    form_class = ReviewForm

    def form_valid(self, form):
        movie = Movie.objects.get(pk=self.kwargs['pk'])
        instance = form.save(commit=False)
        if self.request.POST.get('parent', None):
            instance.parent_id = int(self.request.POST.get('parent'))
        instance.movie = movie
        instance.save()
        return HttpResponseRedirect(movie.get_absolute_url())


class ActorView(GenreYear, DetailView):
    model = Actor
    template_name = 'movies/actor_detail.html'
    slug_field = 'name'


class FilterMoviesView(GenreYear, ListView):
    """
    Фильтр фильмов
    """
    paginate_by = 5

    def get_queryset(self):
        qs = Movie.objects.filter(Q(year__in=self.request.GET.getlist('year')) |
                                  Q(genres__in=self.request.GET.getlist('genre')))\
            .distinct()
        return qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['year'] = ''.join([f'year={x}&' for x in self.request.GET.getlist('year')])
        context['genre'] = ''.join([f'genre={x}&' for x in self.request.GET.getlist('genre')])
        return context


class JsonFilterMoviesView(ListView):
    """
    Фильтр фильмов в json
    """

    def get_queryset(self):
        qs = Movie.objects.filter(Q(year__in=self.request.GET.getlist('year')) |
                                  Q(genres__in=self.request.GET.getlist('genre')))\
            .distinct().values('title', 'tagline', 'url', 'poster')
        return qs

    def get(self, request, *args, **kwargs):
        qs = list(self.get_queryset())
        return JsonResponse({'movies': qs}, safe=False)


class AddStarRating(View):
    """Добавление рейтинга фильму"""

    def get_client_ip(self, request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        return ip

    def post(self, request):
        form = RatingForm(request.POST)
        if form.is_valid():
            Rating.objects.update_or_create(
                ip=self.get_client_ip(request),
                movie_id=int(request.POST.get("movie")),
                defaults={'star_id': int(request.POST.get("star"))}
            )
            return HttpResponse(status=201)
        else:
            return HttpResponse(status=400)


class Search(ListView):
    """ Поиск фильма """

    paginate_by = 5

    def get_queryset(self):
        return Movie.objects.filter(title__icontains=self.request.GET.get('q'))

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['q'] = f'q={self.request.GET.get("q")}&'
        return context
