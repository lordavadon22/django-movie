from django import template

from movies.models import Category, Movie

register = template.Library()


@register.simple_tag
def get_categories():
    return Category.objects.all()


@register.inclusion_tag('tpl/last_movie.html')
def get_last_movies(cnt=3):
    movies = Movie.objects.all()[:cnt]
    return {
        'last_movies': movies
    }
