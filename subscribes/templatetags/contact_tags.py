from django import template

from movies.models import Category, Movie
from subscribes.forms import ContactForm

register = template.Library()


@register.inclusion_tag('tpl/contact_form.html')
def contact_form():
    return {'contact_form': ContactForm()}
