# Django Movie

### Проект еще находится на стадии [dev][1]. Как только появляется время я его допиливаю. 

Для разворачивания данного проекта у себя на сервере нужно:

- Сервер Ubuntu linux 18.04 и выше
- docker версии 20 и выше
- docker-compose версии 2 и выше

## Процедура установки проста:

#### 1) Выполнить клонирование репозитория на сервер

    git clone https://gitlab.com/lordavadon22/django-movie/

#### 2) В корне проекта создать env-dev

    DJANGO_SECRET_KEY=some_secret_key
    DB_NAME=movie
    DB_USER=movie
    DB_PASSWORD=movie
    DB_HOST=db
    DB_PORT=5432

#### 3) В корне проекта создать env-dev_db

    POSTGRES_DB=movie
    POSTGRES_USER=movie
    POSTGRES_PASSWORD=movie

#### 4) Запустить контейнеры

    docker-compose up -d --build

#### 5) Выполнить миграцию базы данных, создать супер пользователя и собрать статику на сервере

    docker-compose exec django_movie python manage.py migrate
    docker-compose exec django_movie python manage.py createsuperuser
    docker-compose exec django_movie python manage.py collectstatic
    
#### 6) Проверить состояние контейнеров

    docker-compose logs -f

#### 7) Можно переходить на сайт по адресу

    http(s)://<ip_адрес_сервера>/

[1]: https://gitlab.com/lordavadon22/django-movie/-/tree/dev